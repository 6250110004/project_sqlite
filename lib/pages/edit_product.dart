import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/food_model.dart';
import 'dart:developer' as developer;

class Edit_Product extends StatefulWidget {
  FoodModel model;

  Edit_Product(this.model);

  @override
  _Edit_ProductState createState() => _Edit_ProductState(model);
}

class _Edit_ProductState extends State<Edit_Product> {
  var name = TextEditingController();
  var price = TextEditingController();

  FoodModel model;
  int? selectedId;

  var _image;
  var imagePicker;
  _Edit_ProductState(this.model);

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    this.selectedId = this.model.id;
    name = TextEditingController()..text = this.model.name;
    price = TextEditingController()..text = this.model.price;
    _image = this.model.image;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
        backgroundColor: Colors.green,
      ),
      body: Container(
        color: Colors.green[100],
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                              source: ImageSource.gallery,
                              imageQuality: 50,
                              preferredCameraDevice: CameraDevice.front);

                          setState(() {
                            _image = image.path;
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                              BoxDecoration(color: Colors.blueAccent[200]),
                          child: _image != null
                              ? Image.file(
                                  File(_image),
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.blueAccent[200]),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                    buildTextfield("name", name),
                    buildTextfield("price", price),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Update'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'Update') {
          await DatabaseHelper.instance.update(
            FoodModel(
                id: selectedId,
                name: name.text,
                price: price.text,
                image: _image),
          );
          setState(() {
            //name.clear();
            selectedId = null;
          });
          Navigator.pop(context);
        } else
          Navigator.pop(context);
      },
      style: ElevatedButton.styleFrom(
          fixedSize: Size(120, 50),
          primary: Colors.green,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
              const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}
