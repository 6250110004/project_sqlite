import 'dart:io';

import 'package:flutter/material.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/food_model.dart';

class Profile_Product extends StatelessWidget {
  final id;
  Profile_Product({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product'),
      ),

      body: Center(
        child: FutureBuilder<List<FoodModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<FoodModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : ListView(
                      children: snapshot.data!.map((food) {
                        return Center(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 90,
                              ),
                              CircleAvatar(
                                backgroundImage: FileImage(File(food.image)),
                                radius: 150,
                              ),
                              SizedBox(
                                height: 60,
                              ),
                              Text('${food.name} : ${food.price} THB', 
                              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                              ),
                              Text('\n\t\tInformation\n You will recive product within 24 hours.',
                                
                              ),
                            ],
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
