import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/data_model.dart';
import 'package:project_sqlite/model/food_model.dart';
import 'package:project_sqlite/pages/about_Dev.dart';
import 'package:project_sqlite/pages/add_product.dart';
import 'package:project_sqlite/pages/edit_product.dart';
import 'package:project_sqlite/pages/profile_product.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              SystemNavigator.pop();
            },
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<List<FoodModel>>(
            future: DatabaseHelper.instance.getGroceries(),
            builder: (BuildContext context,
                AsyncSnapshot<List<FoodModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Favorite Food in List.'))
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Center(
                          child: Card(
                            color: selectedId == grocery.id
                                ? Colors.grey
                                : Colors.blue,
                            child: ListTile(
                              title: Text('${grocery.name} '),
                              subtitle: Text('${grocery.price} บาท'),
                              leading: CircleAvatar(
                                  backgroundImage:
                                      FileImage(File(grocery.image))),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Edit_Product(grocery)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "Do you want to delete this record?"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseHelper.instance
                                                      .remove(grocery.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("Ok"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("Cancel"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                var profileid = grocery.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            Profile_Product(id: profileid)));

                                setState(() {
                                  print(grocery.image);
                                  if (selectedId == null) {
                                    //firstname.text = grocery.firstname;
                                    selectedId = grocery.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                });
                              },
                              onLongPress: () {
                                setState(() {
                                  DatabaseHelper.instance.remove(grocery.id!);
                                });
                              },
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
      drawer: Drawer(
        child: Container(
          color: Color.fromRGBO(255, 205, 0, 1),
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 70),
            children: <Widget>[
              const SizedBox(
                height: 20,
              ),
              buildMenuItem(
                text: 'Product',
                icon: Icons.sports_baseball_rounded,
                onClicked: () => selectedItem(context, 0),
              ),
              const SizedBox(
                height: 20,
              ),
              buildMenuItem(
                text: 'Add , Remove Product',
                icon: Icons.construction_rounded,
                onClicked: () => selectedItem(context, 1),
              ),
              const SizedBox(
                height: 20,
              ),
              buildMenuItem(
                text: 'About',
                icon: Icons.person_rounded,
                onClicked: () => selectedItem(context, 2),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.black;
    final hoverColor = Colors.black;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.pop(context);
        break;
      case 1:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Add_Product()),
        );
        break;
      case 2:
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const About()),
        );
        break;
    }
  }
}
