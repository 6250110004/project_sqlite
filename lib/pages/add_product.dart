import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project_sqlite/database/database_helper.dart';
import 'package:project_sqlite/model/food_model.dart';
import 'package:project_sqlite/pages/home.dart';

class Add_Product extends StatefulWidget {
  Add_Product();

  @override
  _Add_ProductState createState() => _Add_ProductState();
}

class _Add_ProductState extends State<Add_Product> {
  var name = TextEditingController();
  var price = TextEditingController();
  var _image;
  var imagePicker;

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Product'),
        backgroundColor: Colors.yellow,
      ),
      body: Container(
        color: Colors.yellow[100],
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                            source: ImageSource.gallery,
                            imageQuality: 50,
                            //preferredCameraDevice: CameraDevice.front
                          );
                          setState(() {
                            _image = File(image.path);
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                              BoxDecoration(
                                  color: Colors.black,
                                  shape: BoxShape.circle,
                              ),
                          child: _image != null
                              ? Image.file(
                                  _image,
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.fitHeight,
                                )
                              : Container(
                                  decoration:
                                    BoxDecoration(
                                        color: Colors.blue,
                                        shape: BoxShape.circle
                                    ),
                                  width: 200,
                                  height: 200,
                                  child: Icon(
                                    Icons.picture_as_pdf_sharp,
                                    color: Colors.black,
                                  ),
                                ),
                        ),
                      ),
                    ),
                    buildTextfield('Product Name', name),
                    buildTextfield('Price', price),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 30, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('Add Product'),
                          buildElevatedButton('Cancel'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'Add Product') {
          await DatabaseHelper.instance.add(
            FoodModel(name: name.text, price: price.text, image: _image.path),
          );
          setState(() {
            //name.clear();
            //selectedId = null;
          });

          Navigator.pop(context);
        } else
          Navigator.pop(context);
      },
      style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          fixedSize: Size(160, 50),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
              const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}
