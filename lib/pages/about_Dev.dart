import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column();
  }
}

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
   Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Sample App',
      home: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Card(
              child: new Column(
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Container(
                        child: new Image.asset(
                          'assets/images/p.jpg',
                          height: 60.0,
                          fit: BoxFit.cover,
                          
                        ),
                        
                      ),
                      new Container(
                        child: new Text('นายนรพิชญ์ พงษ์สุวรรณ 6250110004 ICM'),
                      ),
                    ],
                  ),
                  new Row(
                    children: <Widget>[
                      new Container(
                        child: new Image.asset(
                          'assets/images/plam.jpg',
                          height: 60.0,
                          fit: BoxFit.cover,
                          
                        ),
                        
                      ),
                      new Container(
                        child: new Text('นายฐนิสพงษ์ ทรัพย์ทวีจรุง 6250110024 ICM'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class name extends StatelessWidget {
  const name({Key? key, required this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.green,
        onPressed: () {},
        child: Row(
          children: [
            SizedBox(width: 20),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}

class image extends StatelessWidget {
  const image({Key? key, required this.assetImage}) : super(key: key);

  final AssetImage assetImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: SizedBox(
        height: 115,
        width: 115,
        child: CircleAvatar(
          backgroundImage: assetImage,
        ),
      ),
    );
  }
}
