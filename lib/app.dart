import 'package:flutter/material.dart';
import 'package:project_sqlite/pages/add_product.dart';
import 'package:project_sqlite/pages/home.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const Home(title: 'Item'),
    );
  }
}
